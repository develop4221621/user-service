FROM openjdk:23-jdk

WORKDIR /app

COPY /target/user-service-0.0.1-SNAPSHOT.jar  /app/user-service-0.0.1-SNAPSHOT.jar

EXPOSE 8085

ENV SPRING_PROFILES_ACTIVE=dev

CMD ["java", "-jar", "user-service-0.0.1-SNAPSHOT.jar"]