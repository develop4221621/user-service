package com.example.userservice.controllers;

import com.example.userservice.models.UserProfile;
import com.example.userservice.services.impl.UserProfileServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/userprofiles")
public class UserProfileController {
    private final UserProfileServiceImpl userProfileService;

    @PostMapping
    public UserProfile createUserProfile(@RequestBody UserProfile userProfile) {
        return userProfileService.createUserProfile(userProfile);
    }

    @GetMapping("/{profileId}")
    public UserProfile getRequestedUserProfile(@PathVariable Long profileId) {
        return userProfileService.getUserProfile(profileId);
    }

    @GetMapping
    public List<UserProfile> allUserProfiles() {
        return userProfileService.getAllUserProfiles();
    }

    @PutMapping("/{id}")
    public UserProfile updateUserProfile(@PathVariable Long id, @RequestBody UserProfile userProfile) {
        return userProfileService.updateUserProfile(id, userProfile);
    }

    @DeleteMapping("/{id}")
    public UserProfile deleteUserProfile(@PathVariable Long id) {
        return userProfileService.deleteUserProfile(id);
    }

}
