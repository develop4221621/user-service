package com.example.userservice.repositories;

import com.example.userservice.entities.UserProfileEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserProfileEntityRepository extends CrudRepository<UserProfileEntity, Long> {
    Optional<UserProfileEntity> findByUserId(Long userId);
}
