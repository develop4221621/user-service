package com.example.userservice.exceptions;

public class AppRuntimeException extends RuntimeException {
    public AppRuntimeException(String exceptionMessage) {
        super(exceptionMessage);
    }
}