package com.example.userservice.exceptions;

import lombok.Data;

@Data
public class ExceptionMessage {
    private String exceptionMessage;
}
