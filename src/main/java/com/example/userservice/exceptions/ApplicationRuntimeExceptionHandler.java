package com.example.userservice.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class ApplicationRuntimeExceptionHandler {
    @ExceptionHandler(AppRuntimeException.class)
    @ResponseBody
    public ExceptionMessage handelAppRuntimeException(AppRuntimeException e){
        ExceptionMessage exceptionMessage = new ExceptionMessage();
        String errorMessage = e.getMessage();
        exceptionMessage.setExceptionMessage(errorMessage);
        log.info("Error occurred during processing:  ",e);
        return exceptionMessage;
    }
}
