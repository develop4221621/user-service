package com.example.userservice.models;

import java.util.HashMap;
import java.util.Map;

public enum UserGender {
    MALE(0), FEMALE(1), OTHER(2);
    private static final Map<Integer, UserGender> valueVsCode = new HashMap<>();
    int value;

    UserGender(int value) {
        this.value = value;
    }

    public static UserGender toStatusCode(int value) {
        if (valueVsCode.isEmpty()) {
            for (UserGender statusCode : UserGender.values()) {
                valueVsCode.put(statusCode.value, statusCode);
            }
        }
        return valueVsCode.get(value);
    }

    public int getValue() {
        return value;
    }
}
