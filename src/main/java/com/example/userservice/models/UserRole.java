package com.example.userservice.models;

import java.util.HashMap;
import java.util.Map;

public enum UserRole {
    USER(0), STAFF(1), ADMIN(2);
    private static final Map<Integer, UserRole> valueVsCode = new HashMap<>();
    int value;

    UserRole(int value) {
        this.value = value;
    }

    public static UserRole toUserRole(int value) {
        if (valueVsCode.isEmpty()) {
            for (UserRole statusCode : UserRole.values()) {
                valueVsCode.put(statusCode.value, statusCode);
            }
        }
        return valueVsCode.get(value);
    }

    public int getValue() {
        return value;
    }


}
