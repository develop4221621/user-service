package com.example.userservice.models;

import com.example.userservice.entities.UserEntity;
import lombok.Data;

@Data
public class User {
    private long id;
    private String username;
    private char[] password;
    private String email;
    private UserRole role;

    public String getPassword() {
        return new String(password == null ? new char[]{} : password);
    }

    public void setPassword(String password) {
        if (password == null) {
            this.password = new char[]{};
        } else {
            this.password = password.toCharArray();
        }
    }

    public UserEntity toEntity() {
        UserEntity entity = new UserEntity();
        entity.setId(this.getId());
        entity.setUsername(this.getUsername());
        entity.setPassword(this.getPassword());
        entity.setEmail(this.getEmail());
        entity.setRole(this.getRole().getValue());
        return entity;
    }

    public User fromEntity(UserEntity entity) {
        this.setId(entity.getId());
        this.setUsername(entity.getUsername());
        this.setPassword("[PASSWORD]");
        this.setEmail(entity.getEmail());
        this.setRole(UserRole.toUserRole(entity.getRole()));
        return this;
    }

}