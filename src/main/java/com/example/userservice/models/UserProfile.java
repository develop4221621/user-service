package com.example.userservice.models;

import com.example.userservice.entities.UserProfileEntity;
import lombok.Data;

import java.util.Date;

@Data
public class UserProfile {
    private long id;
    private long userId;
    private String firstName;
    private String lastName;
    private String phoneNo;
    private String address;
    private Date dateOfBirth;
    private UserGender gender;
    private String profilePicture;

    public UserProfileEntity toEntity() {
        UserProfileEntity entity = new UserProfileEntity();
        entity.setId(this.getId());
        entity.setUserId(this.getUserId());
        entity.setFirstName(this.getFirstName());
        entity.setLastName(this.getLastName());
        entity.setPhoneNumber(this.getPhoneNo());
        entity.setAddress(this.getAddress());
        entity.setDateOfBirth(this.getDateOfBirth());
        entity.setGender(this.getGender().getValue());
        entity.setProfilePicture(this.getProfilePicture());
        return entity;
    }

    public UserProfile fromEntity(UserProfileEntity entity) {
        this.setId(entity.getId());
        this.setUserId(entity.getUserId());
        this.setFirstName(entity.getFirstName());
        this.setLastName(entity.getLastName());
        this.setPhoneNo(entity.getPhoneNumber());
        this.setAddress(entity.getAddress());
        this.setDateOfBirth(entity.getDateOfBirth());
        this.setGender(UserGender.toStatusCode(entity.getGender()));
        this.setProfilePicture(entity.getProfilePicture());
        return this;
    }


}
