package com.example.userservice.services.impl;

import com.example.userservice.entities.UserEntity;
import com.example.userservice.exceptions.AppRuntimeException;
import com.example.userservice.models.User;
import com.example.userservice.repositories.UserEntityRepository;
import com.example.userservice.services.UserService;
import com.example.userservice.util.UniqueIdGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserEntityRepository userEntityRepository;

    private final PasswordEncoder  passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public User createUser(User user) {

        validateUserUniqueness(user);

        user.setId(UniqueIdGenerator.getLongID());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity savedUserEntity = userEntityRepository.save(user.toEntity());
        log.info("User created successfully with username: {} and email: {}", user.getUsername(), user.getEmail());
        return new User().fromEntity(savedUserEntity);
    }

    @Override
    public User updateUser(Long id, User user) {
        Optional<UserEntity> savedUser = userEntityRepository.findById(id);
        if (savedUser.isEmpty()) {
            throw new AppRuntimeException("No such user exist");
        }
        UserEntity updatedUser = userEntityRepository.save(user.toEntity());
        return new User().fromEntity(updatedUser);
    }

    @Override
    public User deleteUser(Long id) {
        Optional<UserEntity> existingUser = userEntityRepository.findById(id);
        if (existingUser.isEmpty()) {
            throw new AppRuntimeException("No such user found to remove");
        }
        userEntityRepository.deleteById(id);
        return new User().fromEntity(existingUser.get());
    }

    @Override
    public User getUser(Long id) {
        Optional<UserEntity> requestedUser = userEntityRepository.findById(id);
        if (requestedUser.isEmpty()) throw new AppRuntimeException("No such user exist with id: " + id);
        return new User().fromEntity(requestedUser.get());
    }

    @Override
    public List<User> getAllUsers() {
        List<User> usersList = new ArrayList<>();
        Iterable<UserEntity> allSavedUsers = userEntityRepository.findAll();
        for (UserEntity entity : allSavedUsers) {
            User newUser = new User().fromEntity(entity);
            usersList.add(newUser);
        }
        return usersList;
    }

    private void validateUserUniqueness(User user) {
        String userEmail = user.getEmail();
        String userName = user.getUsername();
        Optional<UserEntity> existingUserByEmail = userEntityRepository.findByEmail(userEmail);
        Optional<UserEntity> existingUserByUsername = userEntityRepository.findByUsername(userName);
        if (existingUserByEmail.isPresent() || existingUserByUsername.isPresent())
            throw new AppRuntimeException("User with provided username and email already exists " + "username: " + userName + "  email: " + userEmail);
    }
}
