package com.example.userservice.services.impl;

import com.example.userservice.entities.UserProfileEntity;
import com.example.userservice.exceptions.AppRuntimeException;
import com.example.userservice.models.UserProfile;
import com.example.userservice.repositories.UserProfileEntityRepository;
import com.example.userservice.services.UserProfileService;
import com.example.userservice.util.UniqueIdGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserProfileServiceImpl implements UserProfileService {
    private final UserProfileEntityRepository userProfileRepository;

    @Override
    public UserProfile createUserProfile(UserProfile userProfile) {

        validateProfileUniqueness(userProfile);

        userProfile.setId(UniqueIdGenerator.getLongID());
        UserProfileEntity savedUserProfileEntity = userProfileRepository.save(userProfile.toEntity());
        log.info("UserProfile created successfully for userId: {}", userProfile.getUserId());
        return new UserProfile().fromEntity(savedUserProfileEntity);
    }

    @Override
    public UserProfile updateUserProfile(Long id, UserProfile userProfile) {
        Optional<UserProfileEntity> savedUserProfile = userProfileRepository.findById(id);
        if (savedUserProfile.isEmpty()) {
            throw new AppRuntimeException("No such user profile exists");
        }
        UserProfileEntity updatedUserProfile = userProfileRepository.save(userProfile.toEntity());
        return new UserProfile().fromEntity(updatedUserProfile);
    }

    @Override
    public UserProfile deleteUserProfile(Long id) {
        Optional<UserProfileEntity> existingUserProfile = userProfileRepository.findById(id);
        if (existingUserProfile.isEmpty()) {
            throw new AppRuntimeException("No such user profile found to remove");
        }
        userProfileRepository.deleteById(id);
        return new UserProfile().fromEntity(existingUserProfile.get());
    }

    @Override
    public UserProfile getUserProfile(Long id) {
        Optional<UserProfileEntity> requestedUserProfile = userProfileRepository.findById(id);
        if (requestedUserProfile.isEmpty()) throw new AppRuntimeException("No such user profile exists with id: " + id);

        return new UserProfile().fromEntity(requestedUserProfile.get());
    }

    @Override
    public List<UserProfile> getAllUserProfiles() {
        List<UserProfile> userProfilesList = new ArrayList<>();
        Iterable<UserProfileEntity> allSavedUserProfiles = userProfileRepository.findAll();

        for (UserProfileEntity entity : allSavedUserProfiles) {
            UserProfile newUserProfile = new UserProfile().fromEntity(entity);
            userProfilesList.add(newUserProfile);
        }
        return userProfilesList;
    }

    public void validateProfileUniqueness(UserProfile userProfile) {
        Long userId = userProfile.getUserId();
        Optional<UserProfileEntity> savedProfile = userProfileRepository.findByUserId(userId);
        if (savedProfile.isPresent())
            throw new AppRuntimeException("Userprofile of the given userId already exists, userId: " + userId);
    }
}
