package com.example.userservice.services;

import com.example.userservice.models.User;

import java.util.List;

public interface UserService {
    User createUser(User user);

    User updateUser(Long id, User user);

    User deleteUser(Long id);

    User getUser(Long id);

    List<User> getAllUsers();
}