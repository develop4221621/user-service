package com.example.userservice.services;

import com.example.userservice.models.UserProfile;

import java.util.List;

public interface UserProfileService {
    UserProfile createUserProfile(UserProfile userProfile);

    UserProfile updateUserProfile(Long id, UserProfile userProfile);

    UserProfile deleteUserProfile(Long id);

    UserProfile getUserProfile(Long id);

    List<UserProfile> getAllUserProfiles();
}
