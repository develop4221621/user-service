package com.example.userservice.util;

import java.util.UUID;

public class UniqueIdGenerator {
  public static Long getLongID(){
      return System.nanoTime();
  }
  public static String getStringUUID(){
      return UUID.randomUUID().toString();
  }
}
