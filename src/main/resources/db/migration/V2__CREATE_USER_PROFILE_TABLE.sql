CREATE TABLE user_profile
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id         BIGINT,
    first_name      VARCHAR(50) NOT NULL,
    last_name       VARCHAR(50) NOT NULL,
    phone_number    VARCHAR(15),
    address         VARCHAR(255),
    date_of_birth   DATE,
    gender          SMALLINT,
    profile_picture VARCHAR(255),
    FOREIGN KEY (user_id) REFERENCES users (id)
);
